const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");

const scale = 20;
const rows = canvas.height / scale;
const columns = canvas.width / scale;
let SCORE = 0;
let MAIN_ANIMATION = null;
let FPS = 100;

let snake;

(function setup() {
  snake = new Snake();
  const fruit = new Fruit();

  fruit.pickLocation();

  function gameLoop() {
    setTimeout(() => {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      fruit.draw();
      snake.update();
      snake.draw();

      if (snake.eat(fruit)) {
        fruit.pickLocation();
        SCORE++;
        updateScore(SCORE);
      }

      // Movement with arrows
      window.addEventListener("keydown", (event) => {
        const direction = event.key.replace("Arrow", "");
        snake.changeDirection(direction);
      });

      snake.checkCollision();


      MAIN_ANIMATION = window.requestAnimationFrame(gameLoop);
    }, 1000 / FPS)
  }

  gameLoop();

})();

function updateScore(score) {
  const animations = ['shake-horizontal', 'tilt', 'shake-vertical']
  const scoreElement = document.getElementById("score");

  //Balança o jogo
  canvas.classList.add(animations[Math.random() * animations.length | 0]);
  setTimeout(() => {
    canvas.className = '';
  }, 500);


  // Balança o score
  scoreElement.innerHTML = "Score: " + score;
  scoreElement.classList.add(animations[Math.random() * animations.length | 0]);
  setTimeout(() => {
    scoreElement.className = '';
  }, 500);
}

function addScore(nome) {
  const scores = JSON.parse(localStorage.getItem('scores')) || []
  scores.push({ nome, score: SCORE })
  localStorage.setItem('scores', JSON.stringify(scores))

  // calculate the best score
  let lastestScores = JSON.parse(localStorage.getItem('scores')) || []
  lastestScores = scores.sort((a, b) => b.score - a.score)
  return lastestScores[0].score
}

function getScores() {
  const scores = JSON.parse(localStorage.getItem('scores')) || []
  return scores.sort((a, b) => b.score - a.score)
}


function Snake() {
  this.x = 0;
  this.y = 0;
  this.xSpeed = scale;
  this.ySpeed = 0;
  this.tail = [];
  this.total = 0;
  this.color = "#FFFFFF";

  this.canChangeDirection = true;

  this.draw = function () {
    ctx.fillStyle = this.color;

    for (let i = 0; i < this.tail.length; i++) {
      ctx.fillRect(this.tail[i].x, this.tail[i].y, scale, scale);
    }

    ctx.fillRect(this.x, this.y, scale, scale);
  };

  this.changeDirection = function (direction) {
    if (!this.canChangeDirection) {
      return;
    }

    this.canChangeDirection = false;
  }

  this.update = function () {
    this.canChangeDirection = true;
    for (let i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }

    if (this.total > 0) {
      this.tail[this.total - 1] = { x: this.x, y: this.y };
    }

    this.x += this.xSpeed;
    this.y += this.ySpeed;

    if (this.x > canvas.width) {
      this.x = 0;
    }

    if (this.y > canvas.height) {
      this.y = 0;
    }

    if (this.x < 0) {
      this.x = canvas.width;
    }

    if (this.y < 0) {
      this.y = canvas.height;
    }
  };

  this.updateColor = function () {
    const currentColor = this.color.slice(1); // Remove the '#' from the color string
    const currentColorInt = parseInt(currentColor, 16); // Convert the color string to an integer
    const greenColorInt = parseInt("00FF00", 16); // Convert the green color string to an integer
    const newColorInt = currentColorInt + Math.floor((greenColorInt - currentColorInt) * 0.1); // Calculate the new color integer
    this.color = '#' + newColorInt.toString(16).padStart(6, '0'); // Convert the new color integer to a string and add the '#'
  }

  this.eat = function (fruit) {
    if (this.x === fruit.x && this.y === fruit.y) {
      this.total++;
      this.updateColor();
      return true;
    }

    return false;
  };

  this.changeDirection = function (direction) {
    switch (direction) {
      case 'Up':
        if (this.ySpeed !== scale) {
          this.xSpeed = 0;
          this.ySpeed = -scale;
        }
        break;
      case 'Down':
        if (this.ySpeed !== -scale) {
          this.xSpeed = 0;
          this.ySpeed = scale;
        }
        break;
      case 'Left':
        if (this.xSpeed !== scale) {
          this.xSpeed = -scale;
          this.ySpeed = 0;
        }
        break;
      case 'Right':
        if (this.xSpeed !== -scale) {
          this.xSpeed = scale;
          this.ySpeed = 0;
        }
        break;
    }
  };

  this.checkCollision = function () {
    for (let i = 0; i < this.tail.length; i++) {
      if (this.x === this.tail[i].x && this.y === this.tail[i].y) {
        this.total = 0;
        this.tail = [];
        if (confirm("Fim de Jogo! Jogar novamente?")) {
          SCORE = 0;
        } else {
          window.cancelAnimationFrame(MAIN_ANIMATION);
          this.endGame();
        }
      }
    }
  };

  this.endGame = function () {
    const nome = prompt("Digite seu nome para entrar na lista de pontuações: ")

    const bestScore = addScore(nome)
    console.log(bestScore)

    document.body.innerHTML = `
      <div>
        <div style="display: flex; justify-content: center;">
          <img src="sherequi.gif" alt="sherequi dançante"  width="250" />
        </div>
        <p style="font-size: 2.5em; font-weight: bolder; text-align: center;">${nome ? nome : 'Visitante Curioso'}: ${SCORE} ${SCORE == bestScore ? '🏆' : ''
      }</p>
      </div>
      <div>    
        <p style="font-size: 2.5em; font-weight: bolder; text-align: center;">MAIORES PONTUAÇÕES</p>
        <div style="display: flex; justify-content: center;">
          <ol>
          ${getScores().map(({ nome, score }) => `<li style="font-size: 2.5em; font-weight: bolder; text-align: center;">${nome}: ${score}</li>`).join('')}
          </ol>
        </div>     
      </div>
    `;
  }

}

function Fruit() {
  this.x;
  this.y;

  this.pickLocation = function () {
    this.x = Math.floor(Math.random() * columns) * scale;
    this.y = Math.floor(Math.random() * rows) * scale;
  };

  this.draw = function () {
    ctx.fillStyle = "#FF0000";
    ctx.fillRect(this.x, this.y, scale, scale);
  };
}
